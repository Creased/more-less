/**
 * \file main.h
 * \brief More or Less game.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \version 0.1-dev
 * \date 05 September 2017
 */

#ifndef __MAIN_H
#define __MAIN_H

#define MIN_VALUE 1       //!< Minimum value for secret number
#define MAX_VALUE 100     //!< Maximum value for secret number
#define MAX_BUF_LENGTH 5  //!< Maximum buffer length for user input

int generate_secret(const int min, const int max);
void play(const int secret_number, const int min, const int max);
int fget_int(const char message[], char *buf, const int max_buf_size);

#endif // __MAIN_H
