/**
 * \file main.c
 * \brief More or Less game.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \version 0.1-dev
 * \date 05 September 2017
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "main.h"

int main(int argc, char *argv[]) {
    int secret_number;

    //! Generate secret number.
    secret_number = generate_secret(MIN_VALUE, MAX_VALUE);

    //! Let's play!
    play(secret_number, MIN_VALUE, MAX_VALUE);

    printf("You win! Secret number was %d!\n", secret_number);

    return 0;
}

/** Get integer from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return integer Integer number retrieved from user input.
 */
int fget_int(const char message[], char *buf, const int max_buf_size) {
    int integer, finished = 0;
    char junk_char;

    do {
        printf("%s", message);

        //! Fill the buffer with user input and then convert the buffer to integer.
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%d", &integer) != 1) {  // Buffer can't be converted to integer.
                fprintf(stderr, "Please enter a valid integer!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  // Use strlen as index to handle non-full buffer.
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                // printf("Flushing! Removing 0x%x from input.\n", junk_char);
            }
        }
    } while (!finished);

    return integer;
}

/** Play More or Less game.
 *
 * \param secret_number Secret number to find.
 * \param min Minimum value for secret (integer).
 * \param max Maximum value for secret (integer).
 */
void play(const int secret_number, const int min, const int max) {
    int player_number;
    char buf[MAX_BUF_LENGTH];

    printf("More or less? Let's play!\n");
    while (player_number != secret_number) {
        player_number = fget_int("What's your number? ", buf, sizeof(buf));

        if (player_number > max || player_number < min) {
            fprintf(stderr, "Incorrect value! The number must be lower than %d and greater than %d!\n", MAX_VALUE, MIN_VALUE);
        } else if (player_number > secret_number) {
            printf("Less.\n");
        } else if (player_number < secret_number) {
            printf("More.\n");
        }
    }
}

/** Generate a secret number whose both min and max values are defined as parameters.
 *
 * \param min Minimum value for secret (integer).
 * \param max Maximum value for secret (integer).
 *
 * \return secret_number Generated secret number.
 */
int generate_secret(const int min, const int max) {
    int secret_number;

    srand(time(NULL));
    secret_number = (rand() % (max - min + 1)) + min;

    return secret_number;
}
