TARGET = main
CFLAGS = -Wall

all: $(TARGET)

$(TARGET): $(TARGET).c $(TARGET).h
	$(CC) $(CFLAGS) $< -o $(@)

strip: $(TARGET)
	strip $^

clean:
	rm -f $(TARGET)
	rm -rf docs/

docs: Doxyfile
	doxygen $<
